import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppDeepLinkComponent } from './app-deep-link/app-deep-link.component';

const routes: Routes = [
  { path: 'deeplink/:algo/:token', component: AppDeepLinkComponent }
];

@NgModule({
  // imports: [
  //   CommonModule
  // ],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
