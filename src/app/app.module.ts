import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppDeepLinkComponent } from './app-deep-link/app-deep-link.component';

@NgModule({
  declarations: [
    AppComponent,
    AppDeepLinkComponent
  ],
  imports: [
    BrowserModule,    
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
