import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-component-deeplink',
  templateUrl: './app-deep-link.component.html',
  styles: []
})
export class AppDeepLinkComponent implements OnInit {
  token: string;
  algo; string;
  user: any = {};
  isAuthorized = false;

  constructor(
    private _http: HttpClient,
    private route: ActivatedRoute,
    private location: Location,
    private _changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.params['token'];
    this.algo = this.route.snapshot.params['algo'];
    this.verifyToken(this.algo, this.token);
  }

  // Events
  verifyToken = (algo, token) => {
    let headers = new HttpHeaders({ 'jwt': token });
    // headers = headers.append('Access-Control-Allow-Headers', 'JWT');

    // console.log('http headers', headers);

    this._http.get<any>(environment.jwtVerifyEndpoint + algo, { headers: headers })
      .subscribe(
      (response) => {
        // console.log('response', response);
        // control.value = response.token;
        this.isAuthorized = true;
        this.user = response.data;
        this._changeDetectorRef.markForCheck()
      },
      (error) => {
        this.isAuthorized = false;
        console.log('error', error);
      });
  }
}
